import { Engine, Position2D, logger, LogLevel, System } from '../tingine/lib'
import GraphicsEngine from '../tingine/lib/Extras/Graphics/GraphicsEngine'
import GraphicsViewComponent from '../tingine/lib/Extras/Graphics/GraphicsViewComponent'
import Input from '../tingine/lib/Extras/Input/Input'
import StatusBar from '../tingine/lib/Extras/StatusBar/StatusBar'
import { AlignmentType } from '../tingine/lib/Extras/Graphics/AlignmentType'
import { PointerComponent } from '../tingine/lib/Extras/Input/PointerComponent'
import { TextData } from '../tingine/lib/Extras/Graphics/components'
import { Rect } from '../tingine/lib/Extras/Graphics/Rect'

import styles from '../resources/styles.sass'

export const engine = new Engine()

logger.SetLevel(LogLevel.Trace)

export const graphics = new GraphicsEngine(engine)
graphics.canvas.className = styles.canvas
graphics.clearColor = '#222222'

export const statusBar = new StatusBar(engine)
statusBar.dom.className = styles.status

export const input = new Input(engine, graphics.canvas)

export const skin = {
  buttonBgColor: '#A3703A',
  buttonFont: 'DroidSansMono',
  buttonFontSize: 14,
  buttonLabelColor: 'white',
  buttonSprite: 'panel_woodDetail.png',
  labelFontColor: '#A3703A',
  labelFontDecor: '',
  labelFontFace: 'DroidSansMono',
  labelFontSize: 14,
  panelBgColor: '#FFF1D2DD',
  panelSprite: 'panel_woodDetail_blank.png',
}

export type func = () => void

export function createLable(
  pos: Position2D,
  label: string,
  fontSize: number = 16,
  color: string = 'white',
  fontDecorations: string = '',
  onUp: func[] = [],
  rect?: Rect,
) {
  return engine.addEntity([
    pos,
    new GraphicsViewComponent(new TextData({
      alignment: AlignmentType.C,
      color,
      fontDecorations,
      fontFace: 'verdana',
      fontSize,
      label,
    })),
    new PointerComponent(
      [],
      [],
      onUp,
      rect
    )
  ])
}

export const anchor = graphics.anchors.CT

export const clear = () => {
  engine.removeAllEntities()
}

export const menuScene = () => {
  clear()

  createLable(new Position2D(0, 80, anchor), 'Sample', 72, 'white', 'bold')

  createLable(new Position2D(0, 165, anchor), 'Play', 25, 'white', 'bold'
    , [clear, gameScene], new Rect(-50, -10, 100, 20))

  createLable(new Position2D(0, 260, anchor), `
Instructions!
  `.trim(), 16, 'white', '')
}

export const gameScene = () => {
  clear()

  createLable(new Position2D(0, 80, anchor), 'Score: ' + 0, 32, 'white', 'bold')

  createLable(new Position2D(0, 165, anchor), 'Back to Menu', 25, 'white', 'bold'
    , [clear, menuScene], new Rect(-50, -10, 100, 20))
}

engine.addUpdateSystem(class TickSystem extends System {
  components = [GraphicsViewComponent]
  public update(dt: number, _ts: number, _entity: number, g: GraphicsViewComponent) {
    const txtData = g.data as TextData
    if (txtData.data.label.indexOf('Score') !== 0) return

    const score = parseFloat(txtData.data.label.replace(/[^0-9.]/g, ''))
    txtData.data.label = `Score: ` + (score + dt / 1000).toFixed(2)
  }
})